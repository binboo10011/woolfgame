from community import get_communities_by_date_and_weather, get_communities_by_weather
from land import get_all_land_data


if __name__ == "__main__":
    get_communities_by_weather("SUNNY")
    get_communities_by_date_and_weather("2022-12-27", "SUNNY")
    # careful this makes 20K request. the input is a sleep timer amount
    get_all_land_data(.5)