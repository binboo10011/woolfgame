import requests
import json
import csv
from time import sleep
import sys
csv.field_size_limit(sys.maxsize)
def get_com(com_id):
    url = f'https://hdfat7b8eg.execute-api.us-west-2.amazonaws.com/prod/community/{com_id}'

    payload={}
    headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Accept': '*/*',
    'Sec-GPC': '1'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    json_response = json.loads(response.text)
    return json_response


def get_all_com_data():
    with open('./data/community.csv', 'w', newline='') as r:
        spamwriter = csv.writer(r, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for idx in range(100):
            com_id = idx+1
            land = get_com(com_id)
            # print(com_id, land)
            spamwriter.writerow([idx+1, json.dumps(land)])
            sleep(.5)


# @param date ex "2022-12-27"
# @param weather ex"SUNNY"
# prints to {weather}_weather_on_{date}.csv and returns list of tuples ex [[date, community_id, weather]]
def get_communities_by_date_and_weather(date, weather):
    date_weather_list = []
    with open('./data/community.csv', 'r') as f:
        spamreader = csv.reader(f, delimiter=',',
                            quotechar='|')
        with open(f'./data/{weather}_weather_on_{date}.csv', 'a', newline='') as r:
            spamwriter = csv.writer(r, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(["data","community","weater"])
            for rec in spamreader:
                ider = rec[0]
                data = rec[1]
                json_data = json.loads(data)
                forecast = json_data["forecast"]
                date_weather = forecast[date]
                if date_weather == weather:
                    record = [date, ider, date_weather]
                    date_weather_list.append(record)
                    spamwriter.writerow(record)
    return date_weather_list


#  @param weather "SUNNY"
# @returns prints to sunny_weather.csv and returns list of list ex [[date, community_id]]
def get_communities_by_weather(weather):
    date_comm_map = {}
    date_comm_list = []
    with open('./data/community.csv', 'r') as f:
        spamreader = csv.reader(f, delimiter=',',
                            quotechar='|')
        print("date, community, weather")
        for rec in spamreader:
            ider = rec[0]
            data = rec[1]
            json_data = json.loads(data)
            forecast = json_data["forecast"]
            for fore_date in forecast.keys():
                if forecast[fore_date] == weather:
                    if fore_date in date_comm_map and date_comm_map[fore_date] != None:
                        list = date_comm_map[fore_date]
                        list.append(ider)
                        date_comm_map[fore_date] = list
                    else:
                        date_comm_map[fore_date] = [ider]
    with open('./data/sunny_weather.csv', 'w', newline='') as r:
        spamwriter = csv.writer(r, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for date_w in date_comm_map.keys():
            record = [date_w, date_comm_map[date_w]]
            date_comm_list.append(record)
            spamwriter.writerow(record)

    return date_comm_list

