import requests
import json
import csv
from time import sleep
import datetime 

def get_land(land_id):
    url = f'https://hdfat7b8eg.execute-api.us-west-2.amazonaws.com/prod/land/{land_id}'

    payload={}
    headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Accept': '*/*',
    'Sec-GPC': '1'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    json_response = json.loads(response.text)
    return json_response

def create_land_id_string(idx):
    if idx < 10:
        return f'000{idx}'
    if idx < 100:
        return f'00{idx}'
    if idx < 1000:
        return f'0{idx}'
    return f'{idx}'


# @param sleep number ex .5 = half second (defaults to half second)
# @returns printss csv land_{date}.csv and returns list of list ex[[land_id, serialized land blob]]
def get_all_land_data(sleep_num = .5):
    with open(f'./data/land_{datetime.now()}.csv', 'w', newline='') as r:
        spamwriter = csv.writer(r, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for idx in range(20000):
            land_id = create_land_id_string(idx+1)
            land = get_land(land_id)
            record = [idx+1, json.dumps(land)]
            spamwriter.writerow(record)
            sleep(sleep_num)

